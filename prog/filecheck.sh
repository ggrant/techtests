#!/bin/bash
#
# check an important file for changes and notify
#
# Gordon Grant, 05/02/21
#
# Run on CentOS 7 with 'Development Tools' group and 'mailx' RPMs installed.
#
# This method stores the no. of lines in the file and a sha256 hash of the 'stat -t'
# output for the data file in a status file.

# This covers lots of attributes of the file changing (size/mod time/owner, etc.)

#
# Name of the data filename to check
data_file="${HOME}/data_file"

# user to notify
alert_email=root

# file holding current status of datafile
# should hold shell source to set two environment variables.
#
# STAT_HASH : sha256 hash of current 'stat' output of datafile
# NO_LINES : no of lines in the file
#
# This is insecure!

status_file="${HOME}/status"

#
# Main loop

mainloop() {

    # very basic args checking, should use GNU get opts or external shell library to do this properly.
    case $1 in
        -check)

            # get the current status of the files
            get_stat

            # calculate hash of current file state
            calc_curr_status

            if [ "$CURR_HASH" != "$STAT_HASH" ] ; then
                store_new_state
                notify
            fi
            ;;
        -init)

            : > "$status_file"
            ;;
        *)
            bad_args
            ;;

        esac
}

get_stat() {
    if [ -f "${status_file}" ] ; then
        # source the current status
        . "${status_file}"
    fi
    }

calc_curr_status() {
    CURR_HASH=$( stat -t "${data_file}" | sha256sum | gawk '{print $1}' )
    CURR_LINES=$( wc -l "${data_file}" | gawk '{print $1}' )

    if (( CURR_LINES > NO_LINES )); then
        NEW_LINES_CNT=$(( CURR_LINES - NO_LINES ))
    else
        NEW_LINES_CNT=0
    fi
}

store_new_state() {
    # overwrite state file with new state

    # store hash of stat output
    echo "STAT_HASH=${CURR_HASH}" > "$status_file"

    # append count of lines
    echo "NO_LINES=${CURR_LINES}" >> "$status_file"
}

notify() {
    tail -${NEW_LINES_CNT} "${data_file}" | \
        mailx -s "Important data file changed: ${NEW_LINES_CNT} new lines" $alert_email
}

bad_args() {
    echo 'Invalid argument detected - must be either "-init" or "-check"'
    exit 1
}

mainloop "$@"

#
# Answers to questions
#
# 1. Either stop or terminate the instance, or perhaps set a label in AWS against the host, so it is marked as tainted. Plus send an alert out.

# 2. Email is not reliable way to alert someone.
# 2a. It depends on the email system working, plus relies on the email recipient to notice that the email has arrived. Emails aren't guaranteed to be delivered immediately. If hooking emails into automated system, contents of the alert email would have no standard way to contain parseable information, so whole alert method would be brittle.
# 2b. I'd call an alert API (or if need be a Slack, etc.) using e.g. curl calling a POST method over HTTPS.

# 3. The hash of the 'stat -t' output should check most things to do with the file. Perhaps the hash will change even if the file has not been writen when the atime of the file updates (but the filesystem probably defailt to being mounted with 'relatime'). I could play with the output fields of 'stat' to change what is monitored.
