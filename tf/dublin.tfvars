region="eu-west-1"

ami="ami-cdbfa4ab"

vpc-cidr = "10.10.10.0/24"

subnet-cidrs = {
  "a" = "10.10.10.0/27",
  "b" = "10.10.10.32/27",
  "c" = "10.10.10.64/27",
}
