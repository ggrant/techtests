variable "region" {}

variable "vpc-cidr" {}

variable "subnet-cidrs" {
  type = map
}

variable "ami" {}
